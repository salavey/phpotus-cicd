<?php

namespace Shabanov\Otusphp\Connect;

interface ConnectInterface
{
    public function getClient();
}
